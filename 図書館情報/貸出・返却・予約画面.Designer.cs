﻿namespace 図書館情報
{
    partial class 貸出_返却_予約画面
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Loan = new System.Windows.Forms.Button();
            this.Return = new System.Windows.Forms.Button();
            this.Reservation = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ReservationDay = new System.Windows.Forms.TextBox();
            this.UserID = new System.Windows.Forms.TextBox();
            this.BookID = new System.Windows.Forms.TextBox();
            this.LoanDay = new System.Windows.Forms.TextBox();
            this.Execution = new System.Windows.Forms.Button();
            this.ReturnPage = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Comment = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Loan
            // 
            this.Loan.BackColor = System.Drawing.Color.Pink;
            this.Loan.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Loan.Location = new System.Drawing.Point(116, 38);
            this.Loan.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Loan.Name = "Loan";
            this.Loan.Size = new System.Drawing.Size(99, 42);
            this.Loan.TabIndex = 0;
            this.Loan.Text = "貸出";
            this.Loan.UseVisualStyleBackColor = false;
            // 
            // Return
            // 
            this.Return.BackColor = System.Drawing.Color.Pink;
            this.Return.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Return.Location = new System.Drawing.Point(274, 38);
            this.Return.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Return.Name = "Return";
            this.Return.Size = new System.Drawing.Size(99, 42);
            this.Return.TabIndex = 1;
            this.Return.Text = "返却";
            this.Return.UseVisualStyleBackColor = false;
            // 
            // Reservation
            // 
            this.Reservation.BackColor = System.Drawing.Color.Pink;
            this.Reservation.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Reservation.Location = new System.Drawing.Point(422, 38);
            this.Reservation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Reservation.Name = "Reservation";
            this.Reservation.Size = new System.Drawing.Size(99, 42);
            this.Reservation.TabIndex = 2;
            this.Reservation.Text = "予約";
            this.Reservation.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(142, 133);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(75, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "利用者ID";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(142, 168);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(75, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "書籍ID";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(142, 204);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(75, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "貸出日";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(142, 236);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(75, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "返却日";
            // 
            // ReservationDay
            // 
            this.ReservationDay.Location = new System.Drawing.Point(252, 235);
            this.ReservationDay.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ReservationDay.Name = "ReservationDay";
            this.ReservationDay.Size = new System.Drawing.Size(76, 19);
            this.ReservationDay.TabIndex = 6;
            // 
            // UserID
            // 
            this.UserID.Location = new System.Drawing.Point(252, 134);
            this.UserID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.UserID.Name = "UserID";
            this.UserID.Size = new System.Drawing.Size(76, 19);
            this.UserID.TabIndex = 3;
            // 
            // BookID
            // 
            this.BookID.Location = new System.Drawing.Point(252, 169);
            this.BookID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BookID.Name = "BookID";
            this.BookID.Size = new System.Drawing.Size(76, 19);
            this.BookID.TabIndex = 4;
            // 
            // LoanDay
            // 
            this.LoanDay.Location = new System.Drawing.Point(252, 205);
            this.LoanDay.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LoanDay.Name = "LoanDay";
            this.LoanDay.Size = new System.Drawing.Size(76, 19);
            this.LoanDay.TabIndex = 5;
            // 
            // Execution
            // 
            this.Execution.BackColor = System.Drawing.Color.Pink;
            this.Execution.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Execution.Location = new System.Drawing.Point(300, 287);
            this.Execution.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Execution.Name = "Execution";
            this.Execution.Size = new System.Drawing.Size(73, 35);
            this.Execution.TabIndex = 8;
            this.Execution.Text = "実行";
            this.Execution.UseVisualStyleBackColor = false;
            // 
            // ReturnPage
            // 
            this.ReturnPage.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ReturnPage.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ReturnPage.Location = new System.Drawing.Point(300, 346);
            this.ReturnPage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ReturnPage.Name = "ReturnPage";
            this.ReturnPage.Size = new System.Drawing.Size(73, 35);
            this.ReturnPage.TabIndex = 9;
            this.ReturnPage.Text = "戻る";
            this.ReturnPage.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(341, 131);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(75, 18);
            this.label5.TabIndex = 13;
            this.label5.Text = "コメント";
            // 
            // Comment
            // 
            this.Comment.Location = new System.Drawing.Point(368, 154);
            this.Comment.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Comment.Multiline = true;
            this.Comment.Name = "Comment";
            this.Comment.Size = new System.Drawing.Size(134, 112);
            this.Comment.TabIndex = 7;
            // 
            // 貸出_返却_予約画面
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.ClientSize = new System.Drawing.Size(680, 410);
            this.Controls.Add(this.Comment);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ReturnPage);
            this.Controls.Add(this.Execution);
            this.Controls.Add(this.LoanDay);
            this.Controls.Add(this.BookID);
            this.Controls.Add(this.UserID);
            this.Controls.Add(this.ReservationDay);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Reservation);
            this.Controls.Add(this.Return);
            this.Controls.Add(this.Loan);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "貸出_返却_予約画面";
            this.Text = "貸出_返却_予約画面";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Loan;
        private System.Windows.Forms.Button Return;
        private System.Windows.Forms.Button Reservation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ReservationDay;
        private System.Windows.Forms.TextBox UserID;
        private System.Windows.Forms.TextBox BookID;
        private System.Windows.Forms.TextBox LoanDay;
        private System.Windows.Forms.Button Execution;
        private System.Windows.Forms.Button ReturnPage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Comment;
    }
}