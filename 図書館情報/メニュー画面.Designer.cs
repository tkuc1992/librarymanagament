﻿namespace 図書館情報
{
    partial class メニュー画面
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.List = new System.Windows.Forms.Button();
            this.Function = new System.Windows.Forms.Button();
            this.Logout = new System.Windows.Forms.Button();
            this.End = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.UserId = new System.Windows.Forms.TextBox();
            this.BookId = new System.Windows.Forms.TextBox();
            this.Loan = new System.Windows.Forms.TextBox();
            this.Return1 = new System.Windows.Forms.TextBox();
            this.Reservation = new System.Windows.Forms.TextBox();
            this.Return2 = new System.Windows.Forms.TextBox();
            this.UsageStats = new System.Windows.Forms.TextBox();
            this.Display = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // List
            // 
            this.List.BackColor = System.Drawing.Color.Pink;
            this.List.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.List.Location = new System.Drawing.Point(210, 50);
            this.List.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.List.Name = "List";
            this.List.Size = new System.Drawing.Size(114, 41);
            this.List.TabIndex = 1;
            this.List.Text = "本・利用者一覧検索";
            this.List.UseVisualStyleBackColor = false;
            // 
            // Function
            // 
            this.Function.BackColor = System.Drawing.Color.Pink;
            this.Function.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Function.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Function.Location = new System.Drawing.Point(41, 50);
            this.Function.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Function.Name = "Function";
            this.Function.Size = new System.Drawing.Size(114, 41);
            this.Function.TabIndex = 0;
            this.Function.Text = "貸出・返却・予約";
            this.Function.UseVisualStyleBackColor = false;
            // 
            // Logout
            // 
            this.Logout.BackColor = System.Drawing.Color.Pink;
            this.Logout.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Logout.Location = new System.Drawing.Point(380, 50);
            this.Logout.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(114, 41);
            this.Logout.TabIndex = 2;
            this.Logout.Text = "ログアウト";
            this.Logout.UseVisualStyleBackColor = false;
            // 
            // End
            // 
            this.End.BackColor = System.Drawing.Color.LightSkyBlue;
            this.End.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.End.Location = new System.Drawing.Point(534, 50);
            this.End.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.End.Name = "End";
            this.End.Size = new System.Drawing.Size(114, 41);
            this.End.TabIndex = 3;
            this.End.Text = "アプリ終了";
            this.End.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(9, 116);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(75, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "利用者ID";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(9, 150);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(75, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "書籍ID";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(169, 115);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(75, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "貸出日";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(169, 150);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(75, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "返却日";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(328, 115);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(70, 18);
            this.label5.TabIndex = 5;
            this.label5.Text = "予約日";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(328, 149);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(73, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "返却予定日";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(484, 115);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(75, 18);
            this.label7.TabIndex = 7;
            this.label7.Text = "貸出状況";
            // 
            // UserId
            // 
            this.UserId.Location = new System.Drawing.Point(89, 116);
            this.UserId.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.UserId.Name = "UserId";
            this.UserId.Size = new System.Drawing.Size(76, 19);
            this.UserId.TabIndex = 4;
            // 
            // BookId
            // 
            this.BookId.Location = new System.Drawing.Point(89, 150);
            this.BookId.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BookId.Name = "BookId";
            this.BookId.Size = new System.Drawing.Size(76, 19);
            this.BookId.TabIndex = 8;
            // 
            // Loan
            // 
            this.Loan.Location = new System.Drawing.Point(249, 116);
            this.Loan.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Loan.Name = "Loan";
            this.Loan.Size = new System.Drawing.Size(76, 19);
            this.Loan.TabIndex = 5;
            // 
            // Return1
            // 
            this.Return1.Location = new System.Drawing.Point(249, 150);
            this.Return1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Return1.Name = "Return1";
            this.Return1.Size = new System.Drawing.Size(76, 19);
            this.Return1.TabIndex = 9;
            // 
            // Reservation
            // 
            this.Reservation.Location = new System.Drawing.Point(404, 116);
            this.Reservation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Reservation.Name = "Reservation";
            this.Reservation.Size = new System.Drawing.Size(76, 19);
            this.Reservation.TabIndex = 6;
            // 
            // Return2
            // 
            this.Return2.Location = new System.Drawing.Point(404, 149);
            this.Return2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Return2.Name = "Return2";
            this.Return2.Size = new System.Drawing.Size(76, 19);
            this.Return2.TabIndex = 10;
            // 
            // UsageStats
            // 
            this.UsageStats.Location = new System.Drawing.Point(564, 116);
            this.UsageStats.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.UsageStats.Name = "UsageStats";
            this.UsageStats.Size = new System.Drawing.Size(76, 19);
            this.UsageStats.TabIndex = 7;
            // 
            // Display
            // 
            this.Display.BackColor = System.Drawing.Color.Pink;
            this.Display.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Display.Location = new System.Drawing.Point(564, 178);
            this.Display.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(84, 20);
            this.Display.TabIndex = 11;
            this.Display.Text = "表示";
            this.Display.UseVisualStyleBackColor = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(41, 201);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(607, 175);
            this.dataGridView1.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("UD デジタル 教科書体 NP-B", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.ForeColor = System.Drawing.Color.Violet;
            this.label8.Location = new System.Drawing.Point(43, 177);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 22);
            this.label8.TabIndex = 15;
            this.label8.Text = "一覧";
            // 
            // メニュー画面
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.ClientSize = new System.Drawing.Size(680, 410);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Display);
            this.Controls.Add(this.UsageStats);
            this.Controls.Add(this.Return2);
            this.Controls.Add(this.Reservation);
            this.Controls.Add(this.Return1);
            this.Controls.Add(this.Loan);
            this.Controls.Add(this.BookId);
            this.Controls.Add(this.UserId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.End);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.Function);
            this.Controls.Add(this.List);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "メニュー画面";
            this.Text = "メニュー画面";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button List;
        private System.Windows.Forms.Button Function;
        private System.Windows.Forms.Button Logout;
        private System.Windows.Forms.Button End;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox UserId;
        private System.Windows.Forms.TextBox BookId;
        private System.Windows.Forms.TextBox Loan;
        private System.Windows.Forms.TextBox Return1;
        private System.Windows.Forms.TextBox Reservation;
        private System.Windows.Forms.TextBox Return2;
        private System.Windows.Forms.TextBox UsageStats;
        private System.Windows.Forms.Button Display;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label8;
    }
}